<!DOCTYPE html>
<html lang="en">

<head>
    <!--LIBRERIAS-->
    <?php include 'php/librerias.php'; ?>
    <!--FAVICON-->
    <title>Telediabetes</title>
    <link rel="icon" type="image/png" href="img/diabetesfavicon.png">
</head>
<body>

    <!--NAVBAR-->
    <?php include 'php/nav.php'; ?>

    <!--FOTOS CAROUSEL-->

    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="img/pablita-596.png" alt="1" width="1200" height="700">
                <div class="carousel-caption">
                </div>
            </div>

            <div class="item">
                <img src="img/pablita-healthy-life-1.png" alt="2" width="1200" height="700">
                <div class="carousel-caption">
                </div>
            </div>

            <div class="item">
                <img src="img/pablita-online-doctor-consultation.png" alt="3" width="1200" height="700">
                <div class="carousel-caption">
                </div>
            </div>
        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control" style="background-image: none;" href="#myCarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" style="background-image: none;" href="#myCarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

    <!--CONTAINER CENTROS DE SALUD-->
    <div class="container text-center">
        <h3>Centros de salud asociados:</h3>
        <p>El centro de salud familiar santa teresa de Los andes es un establecimiento de atención PRIMARIA de salud que trabaja con la comunidad con un enfoque familiar y desarrolla actividades de promoción, prevención,curación y rehabilitación de la salud.</p>
        <br>
        <div class="row">
            <div class="col-sm-4">
                <p class="text-center"><strong>Cesfam "Santa Teresa de los Andes"</strong></p><br>
                <a href="#demo" data-toggle="collapse">
                    <img src="img/diabetesfavicon.png" class="img-circle person" alt="Random Name" width="255" height="255">
                </a>
                <div id="demo" class="collapse">
                    <p>
                        <b>Dirección:</b> Pasaje Huara 5379 (ex. Gustavo Campaña 5380).
                        <br>
                        <b>Teléfono:</b> 22 576 1163 / 51 / 53
                        <br>
                        <b>Email:</b> cesfamsorteresa@gmail.com
                    </p>
                </div>
            </div>
            <div class="col-sm-4">
                <p class="text-center"><strong>Cecof "Coñimo"</strong></p><br>
                <a href="#demo2" data-toggle="collapse">
                    <img src="img/diabetesfavicon.png" class="img-circle person" alt="Random Name" width="255" height="255">
                </a>
                <div id="demo2" class="collapse">
                    <p>
                        <b>Dirección:</b> Catruman 275,San Joaquín,Región Metropolitana.
                        <br>
                        <b>Teléfono:</b> (2) 2221 0557
                        <br>
                    </p>
                </div>
            </div>
            <div class="col-sm-4">
                <p class="text-center"><strong>Cecof "Salvador Allende"</strong></p><br>
                <a href="#demo3" data-toggle="collapse">
                    <img src="img/diabetesfavicon.png" class="img-circle person" alt="Random Name" width="255" height="255">
                </a>
                <div id="demo3" class="collapse">
                    <p>
                        <b>Dirección:</b> Juan Aravena 472,San Joaquín,Región Metropolitana.
                        <br>
                        <b>Teléfono:</b> (2) 2576 9930
                        <br>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <!-- CONTAINER SOBRE NOSOTROS(?) -->
    <div class="bg-1 " style="background-color: #14aad6;">
        <div class="container">
            <h3 class="text-center">Sobre nosotros</h3>
            <p class="text-center">este proyecto fue creado por y para blah blah blah</p>

            </ul>
        </div>
    </div>


    <!--FOOTER-->
    <footer id="mainFooter">
        <div class="footer-left">
            <img src="img/diabetesfavicon.png" width="100" height="100">
        </div>
        <strong>
            <center><b>Tele</b>diabetes</a></center>
        </strong>
        <div class="footer-right">
            <ul class="social-nav">
                <li>Follow Us</li>
                <li class="icon-bubble"><a href="https://www.facebook.com/Telediabetes" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                <li class="icon-bubble"><a href="https://twitter.com/Telediabetes" target="_blank"><i class="fab fa-twitter"></i></a></li>
                <li class="icon-bubble"><a href="https://instagram.com/Telediabetes" target="_blank"><i class="fab fa-instagram"></i></a></li>
                <li class="icon-bubble"><a href="https://youtube.com/Telediabetes" target="_blank"><i class="fab fa-youtube"></i></a></li>
            </ul>
        </div>
    </footer>
    <center>
        <p>© Telediabetes DuocUC 2020.</p>
    </center>

</body>

</html>