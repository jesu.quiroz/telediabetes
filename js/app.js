$(document).ready(function(){
    $("#validacion-correo").validate({
        errorClass:"is-invalid",
        rules:{
            correo:{
                required: true,
                email: true
            },
        },
        messages:{
            correo:{
                required:"Enter a valid email.",
                email:"You must enter a valid email."
            },
        }
    })
})
$("#validacion-correo").submit(function(){
    if($("#validacion-correo").valid()){
        return true
    }else{
        Swal.fire({
            type: 'error',
            title: 'Ups!',
            text: 'Please enter a valid email.',
          })
    } 
    return false
})

$(document).ready(function(){
    $("#correo-pass").validate({
        errorClass:"is-invalid",
        rules:{
            correo:{
                required: true,
                email: true
            },
            contra:{
                required: true,
                password: true
            },
        },
        messages:{
            correo:{
                required:"Please enter a valid email.",
                email:"Please enter a valid email."
            },
            contra:{
                required: "Please enter a valid password.",
                password: "Please enter a valid password."
            },

        }
    })
})

$("#correo-pass").submit(function(){
    if($("#correo-pass").valid()){
        return true
    }else{
        Swal.fire({
            type: 'error',
            title: 'Ups!',
            text: 'Missing email or password.',
          })
    } 
    return false
})

$(document).ready(function(){
    validation = new Date()
    validation.setFullYear(validation.getFullYear()-12)
    format= `${validation.getFullYear()}-${(validation.getMonth()+1)}-${validation.getDate()}`
    console.log(format)
    $("#crear-cuenta").validate({
        errorClass:"is-invalid",
        rules:{
            nombre:{
                required:true
            },
            correo:{
                required: true,
                email: true
            },
            contra:{
                required: true,
                password: true
            },
            nick:{
                required:true
            },
            cumple:{
                required:true,
                max:format
            }, 
        },
        messages:{
            correo:{
                required:"Enter a valid email.",
                email:"You must enter a valid email."
            },
            contra:{
                required: "Enter password.",
                password: "no sé que poner."
            },
            nombre:{
                required:"Enter your name."
            },
            nick:{
                required:"Enter your nickname."
            },
            cumple:{
                required:"Enter your birthday.",
                max:"You have to be over twelve years old."
            }, 
        }
    })
})

$("#crear-cuenta").submit(function(){
    if($("#crear-cuenta").valid()){
        return true
    }else{
        Swal.fire({
            type: 'error',
            title: 'Ups!',
            text: 'One or more requested fields are missing.',
          })
    } 
    return false
})