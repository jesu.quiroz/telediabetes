<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="generator" content="Jekyll v4.1.1">
    <!-- librerias css -->
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link href="css/sweetalert2.css">
    <link rel="stylesheet" href="css/style.css">
    <!-- librerias js -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="js/sweetalert2.all.js"></script>

    <!--FAVICON-->
    <title>Registro</title>
    <link rel="icon" type="image/png" href="img/diabetesfavicon.png">


    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
        
        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
        
        .fondo-InicioSesion {
            background-image: url("img/fondoins.jpg");
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: cover;
        }
    </style>
    <script>
        $(document).ready(function() {
            function nd() {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Debes ingresar todos los datos!',
                    footer: ''
                })
            }
            $("#form-registro").submit(function(event) {
                
                var rut = $("#rut").val();
                var pass = $("#pass").val();
                var nombre = $("#nombre").val();
                var apellido = $("#apellido").val();
                var fnac = $("#fnac").val();
                var correo = $("#correo").val();
                if (rut == '' || pass == '' || nombre == '' || apellido == '' || fnac == '' || correo == '') {
                    nd();
                    event.preventDefault();
                }
            });


        })
    </script>

</head>

<body class="text-center fondo-InicioSesion">
    <form class="form-signin" id="form-registro" method="POST" action="php/conexionBBDD/regbbdd.php">
        <img class="mb-4" src="img/diabetesfavicon.png" alt="" width="72" height="72">
        <h1 class="h3 mb-3 font-weight-normal">Registro</h1>
        <br>
        <div class="form-group row">
            <label for="example-text-input" class="col-2 col-form-label">Rut </label>
            <div class="col-10">
                <input class="form-control" type="text" placeholder="Rut" id="rut" name="rut" minlength="7" maxlength="9">
                <small id="rutHelp" class="form-text text-muted">Ingresar rut sin puntos ni guión.</small>
            </div>
        </div>

        <div class="form-group row">
            <label for="example-text-input" class="col-2 col-form-label">Nombre </label>
            <div class="col-10">
                <input class="form-control" type="text" placeholder="Nombre" id="nombre" name="nombre">
            </div>
        </div>

        <div class="form-group row">
            <label for="example-text-input" class="col-2 col-form-label">Apellido </label>
            <div class="col-10">
                <input class="form-control" type="text" placeholder="Apellido" id="apellido" name="apellido" >
            </div>
        </div>

        <div class="form-group row">
            <label for="example-date-input" class="col-2 col-form-label">Fecha Nacimiento</label>
            <div class="col-10">
                <input class="form-control" type="date" placeholder="2011-08-19" id="fnac" name="fnac">
            </div>
        </div>

        <div class="form-group row">
            <label for="example-email-input" class="col-2 col-form-label">Correo electrónico </label>
            <div class="col-10">
                <input class="form-control" type="email" placeholder="Correo electrónico" id="correo" name="correo">
            </div>
        </div>

        <div class="form-group row">
            <label for="example-password-input" class="col-2 col-form-label">Contraseña</label>
            <div class="col-10">
                <input class="form-control" type="password" placeholder="*******" id="pass" name="pass">
            </div>
        </div>

        <div class="form-group row">
            <label for="selectgenero">Género</label>
            <select class="form-control" id="selectgenero" name="selectgenero">
      <option value="Femenino">Femenino</option>
      <option value="Masculino">Masculino</option>
      <option value="Otro">Otro</option>
    </select>
        </div>

        <div class="form-group row">
            <label for="selectcentro">Centro de Salud</label>
            <select class="form-control" id="selectcentro" name="selectcentro">
      <option value="CESFAM Sor Teresa de los Andes">CESFAM Sor Teresa de los Andes</option>
      <option value="CECOF Coñimo"> CECOF Coñimo </option>
      <option value="CECOF Juan Aravena"> CECOF Juan Aravena </option>
    </select>
        </div>
        <br>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Registrar</button>
        <br>
        <a href="index.php" class="btn btn-lg btn-primary btn-block btn-info" role="button">Volver</a>
    </form>
</body>

</html>