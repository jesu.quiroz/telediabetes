<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="generator" content="Jekyll v4.1.1">
    <!-- librerias css -->
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link href="css/sweetalert2.css">
    <link rel="stylesheet" href="css/style.css">
    <!-- librerias js -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="js/sweetalert2.all.js"></script>


    <!--FAVICON-->
    <title>Inicio de Sesión</title>
    <link rel="icon" type="image/png" href="img/diabetesfavicon.png">


    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
        
        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
        
        .fondo-InicioSesion {
            background-image: url("img/fondoins.jpg");
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: cover;
        }
    </style>
    <script>
        function checkRut(rut) {
            // Despejar Puntos
            var valor = rut.value.replace('.', '');
            // Despejar Guión
            valor = valor.replace('-', '');

            // Aislar Cuerpo y Dígito Verificador
            cuerpo = valor.slice(0, -1);
            dv = valor.slice(-1).toUpperCase();

            // Formatear RUN
            rut.value = cuerpo + '-' + dv

            // Si no cumple con el mínimo ej. (n.nnn.nnn)
            if (cuerpo.length < 7) {
                rut.setCustomValidity("RUT Incompleto");
                return false;
            }

            // Calcular Dígito Verificador
            suma = 0;
            multiplo = 2;

            // Para cada dígito del Cuerpo
            for (i = 1; i <= cuerpo.length; i++) {

                // Obtener su Producto con el Múltiplo Correspondiente
                index = multiplo * valor.charAt(cuerpo.length - i);

                // Sumar al Contador General
                suma = suma + index;

                // Consolidar Múltiplo dentro del rango [2,7]
                if (multiplo < 7) {
                    multiplo = multiplo + 1;
                } else {
                    multiplo = 2;
                }

            }

            // Calcular Dígito Verificador en base al Módulo 11
            dvEsperado = 11 - (suma % 11);

            // Casos Especiales (0 y K)
            dv = (dv == 'K') ? 10 : dv;
            dv = (dv == 0) ? 11 : dv;

            // Validar que el Cuerpo coincide con su Dígito Verificador
            if (dvEsperado != dv) {
                rut.setCustomValidity("RUT Inválido");
                return false;
            }

            // Si todo sale bien, eliminar errores (decretar que es válido)
            rut.setCustomValidity('');
        }
        $(document).ready(function() {
            function nd() {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Debes ingresar todos los datos!',
                    footer: '<a href="registro.php">Tambien puedes registrarte</a>'
                })
            }
            $("#form-entrar").submit(function(event) {
                event.preventDefault();
                var rut = $("#rut").val();
                var pass = $("#pass").val();
                if (rut == '' || pass == '') {
                    nd();
                    return;
                }
            }); 
        })
    </script>

</head>

<body class="text-center fondo-InicioSesion">
    <form class="form-signin" id="form-entrar" method="POST" action="autenticar.php">
        <img class="mb-4" src="img/diabetesfavicon.png" alt="" width="72" height="72">
        <h1 class="h3 mb-3 font-weight-normal">Iniciar Sesión</h1>
        <br>

        <input type="text" id="rut" class="form-control" placeholder="Rut" maxlength="10" oninput="checkRut(this)" autofocus>
        <br>

        <input type="password" id="contraseña" class="form-control" placeholder="Contraseña">
        <div class="checkbox mb-3">
            <label>
    <input type="checkbox" value="remember-me"> Recordar datos
  </label>


        </div>
        <br>
        <button class="btn btn-lg btn-primary btn-block" type="submit">ingresar</button>
        <br>
        <a href="/" class="btn btn-lg btn-primary btn-block btn-info" role="button">Volver</a>
    </form>

</body>

</html>