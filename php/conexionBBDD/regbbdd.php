<?php
include 'conect.php';
$conexion = new mysqli($host_db, $user_db, $pass_db, $db_name);
if ($conexion->connect_error) {
    die("La conexion falló: " . $conexion->connect_error);
}
mysqli_set_charset($conexion, "utf8");

//Vemos si ya se registro
$buscarUsuario = "SELECT * FROM paciente WHERE rut = '$_POST[rut]' ";
$result = $conexion->query($buscarUsuario);
$count = mysqli_num_rows($result);
if ($count == 1) {
    echo "<br />" . "Usuario ya existente." . "<br />";
    echo "<a href='iniciosesion.php'>iniciar sesion</a>";
} else {
    //repetimos el proceso para ver si existe en la validacion
    $buscarUsuario = "SELECT * FROM val_paciente WHERE Rut_regis = '$_POST[rut]' ";
    $result = $conexion->query($buscarUsuario);
    $count = mysqli_num_rows($result);
    if ($count == 0) {
        echo "<br />" . "No puedes registrarte." . "<br />";
        echo "<a href='index.php'>Regresar</a>";
    } else {
        $query = "INSERT INTO `paciente`(`Rut`, `Nombre`, `Apellido`, `Fecha_nacimiento`, `Correo_electronico`, `Contraseña`, `Genero`, `Centro_salud`)
                                VALUES ('$_POST[rut]','$_POST[nombre]','$_POST[apellido]','$_POST[fnac]','$_POST[correo]','$_POST[pass]','$_POST[selectgenero]','$_POST[selectcentro]')";
        if ($conexion->query($query) === TRUE) {
            echo "<br />" . "<h2>" . "Usuario Creado Exitosamente!" . "</h2>";
            echo "<h4>" . "Bienvenido: " . $_POST['nombre'] . "</h4>" . "Redireccionando, espere...";
            echo '<img src="/images/loading.gif" height="70px" width="70px"></img>';
            header('refresh:2; url=/index.php');
        } else {
            echo "Error al crear el usuario." . $query . "<br>" . $conexion->error;
        }
    }
}

mysqli_close($conexion);
