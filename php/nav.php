<nav class="navbar navbar-default" style="background-color: white;">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">Telediabetes</a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#">Home</a></li>
                    <li><a href="#">Inicio</a></li>
                    <li><a href="#">Centros de Salud</a></li>
                    <li><a href="#">Sobre Nosotros</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="registro.php"><span class="glyphicon glyphicon-user"></span> Registro</a></li>
                    <li><a href="iniciosesion.php"><span class="glyphicon glyphicon-log-in"></span> Inicio Sesion</a></li>
                </ul>
            </div>
        </div>
    </nav>