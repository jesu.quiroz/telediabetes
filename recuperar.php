<!DOCTYPE html>
<html lang="es-cl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/flexboxgrid.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0-12/css/all.css">
    <link href="https://fonts.googleapis.com/css?family=Space+Mono&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">

    <!--FAVICON-->
    <title></title>
    <link rel = "icon" type = "image/png" href = "static\proyecto\img/diabetesfavicon.png">

</head>
<body class="grad">
    <div class="row header middle-xs  color-fondo ">
        <div class="col-xs-12 col-sm-6 acceso-centro">
            <div class=" acceso-centro">
                <img src="static\proyecto\img/024-astronaut.png" width="100" height="100" align="left" class="d-inline-block align-top" alt=""> 
                    <h1 class="letra-logo"><b>Forgot</b>Password</h1>
            </div>
 </div>
</div>
<div >
<div class="container color-fondo-acceso col-lg-4 ">
        <form action="index.html" method="GET" id="validacion-correo">
                <div class="form-group ">
                  <label for="correo">We will send an email to retrieve your password.</label>
                  <input type="email" class="form-control"  name="correo"  placeholder="Enter email" required>
                </div>
                <button type="submit"  class="btn btn-primary">Send</button>
        </form>
</div>
</div>
    <!-- JS FILES-->
<script src="js/jquery-3.4.1.js"></script>
<script src="js/jquery.validate.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script>

<script src="js/bootstrap.js"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="js/app.js"></script>
</body>
</html>